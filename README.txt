-- Ubercart CECA --
This module includes TPV for spanish banks that use ceca secure server.

-- Install & Config --
Enable the module.
Add the data provided by your bank in 
Store administration > Configuration > Payment > Edit > CECA settings
or admin/store/settings/payment/edit/uc_ceca

-- Collaborate --
If you want to help with this module, you can submit patches, find bugs and
communicate them through the issue queue or even your own ceca module 
for a bank in particular

If you want new banks supported & tested, or need any special support, 
you can contact with Neurotic (http://neurotic.es) 