<?php

/**
 * @file
 * Admin functions for uc ceca.
 */

/**
 * Display all the banks with its possible operations.
 */
function uc_ceca_admin_settings() {
  $banks = uc_ceca_get_all_banks();
  $rows = array();
  $output = "";
  foreach ($banks as $bank_details) {
    $row = array();
    $row[] = $bank_details['bankname'];
    $row[] = $bank_details['bankmode'];
    if (!$bank_details['enabled']) {
      $row[] = t('Disabled');
    }
    else {
      $row[] = t('Enabled');
    }
    $row[] = l(t('Edit'), 'admin/store/settings/uc_ceca/edit/'. $bank_details['bankcode']);
    $row[] = l(t('Delete'), 'admin/store/settings/uc_ceca/delete/'. $bank_details['bankcode']);
    $row[] = l(t('Export'), 'admin/store/settings/uc_ceca/export/'. $bank_details['bankcode']);
    $rows[] = $row;
  }
  if (count($rows)) {
    $header = array(t('Bank Name'), t('Mode'), t('Enabled'), array('data' => t('Operations'), 'colspan' => 3));
    $output .= theme('table', $header, $rows);
  }
  else {
    $output .= t('There are no banks configured, you should start adding one.');
  }
  drupal_set_message(t('These are the banks you have available, you can also <a href="@addone">add one</a> or <a href="@importone">import one</a>',
    array('@addone' => url('admin/store/settings/uc_ceca/add'), '@importone' => url('admin/store/settings/uc_ceca/import'))));
  return $output;
}

/**
 * Form to manage the banks.
 */
function uc_ceca_form_bank($form_state, $mode = NULL) {
  $form = array();
  $bankcode = arg(5);
  //Determine the default values  
  $bank_defaults = new stdClass;
  $bank_defaults->enabled = 0;
  $bank_defaults->encryption = 'sha1';
  $bank_defaults->bankmode = 'test';
  $bank_defaults->currency = 978;
  $bank_defaults->Exponente = 2;
  $bank_defaults->Pago_soportado = 'SSL';
  if ($mode == 'edit' && $bankcode) {
    $bank_defaults = uc_ceca_get_bank($bankcode);
  }
  $form['bankcode'] = array(
    '#type' => 'textfield',
    '#description' => t('Codename for the bank, must be unique'),
    '#title' => t('Bank Code'),
    '#default_value' => $bank_defaults->bankcode,
    '#size' => 80,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  if ($bank_defaults->bankcode) {
    $form['bankcode']['#disabled'] = TRUE;
    $form['bankcode']['#value'] = $bank_defaults->bankcode;
  }
  $form['bankname'] = array(
    '#type' => 'textfield',
    '#title' => t('Bank Name'),
    '#default_value' => $bank_defaults->bankname,
    '#size' => 80,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['bankmode'] = array(
    '#type' => 'radios',
    '#title' => t('Mode of the bank'),
    '#default_value' => $bank_defaults->bankmode,
    '#options' => array('test' => t('Test'), 'live' => t('Live')),
    '#size' => 10,
    '#maxlength' => 5,
    '#required' => TRUE,
  );
  $form['Ds_Merchant_Titular'] = array(
    '#type' => 'textfield',
    '#title' => t('Ds_Merchant_Titular'),
    '#default_value' => $bank_defaults->Ds_Merchant_Titular,
    '#size' => 80,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['MerchantID'] = array(
    '#type' => 'textfield',
    '#title' => t('MerchantID'),
    '#default_value' => $bank_defaults->MerchantID,
    '#size' => 80,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['Firma'] = array(
    '#type' => 'textfield',
    '#title' => t('Clave de Encriptación'),
    '#default_value' => $bank_defaults->Firma,
    '#size' => 80,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['TerminalID'] = array(
    '#type' => 'textfield',
    '#title' => t('TerminalID'),
    '#default_value' => $bank_defaults->TerminalID,
    '#size' => 8,
    '#maxlength' => 8,
    '#required' => TRUE,
  );
  $form['Ds_Merchant_ProductDescription'] = array(
    '#type' => 'textfield',
    '#title' => t('Ds_Merchant_ProductDescription'),
    '#default_value' => $bank_defaults->Ds_Merchant_ProductDescription,
    '#size' => 80,
    '#maxlength' => 255,
  );
  $form['currency'] = array(
    '#type' => 'radios',
    '#title' => t('Currency'),
    '#default_value' => $bank_defaults->currency,
    '#options' => array(
      '978' => t('Euro'),
      '840' => t('Dollar'),
      '826' => t('Pound'),
    ),
    '#required' => TRUE,
  );
  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Bank connect url'),
    '#default_value' => $bank_defaults->url,
    '#size' => 80,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
    $form['AcquirerBIN'] = array(
    '#type' => 'textfield',
    '#title' => t('AcquirerBIN'),
    '#default_value' => $bank_defaults->AcquirerBIN,
    '#size' => 10,
    '#maxlength' => 10,
    '#required' => TRUE,
  );
  $form['Exponente'] = array(
    '#type' => 'textfield',
    '#title' => t('Exponente'),
    '#default_value' => (isset($bank_defaults->Exponente)) ? $bank_defaults->Exponente: 'SSL',
    '#size' => 1,
    '#maxlength' => 1,
    '#required' => TRUE,
  );
    $form['Pago_soportado'] = array(
    '#type' => 'textfield',
    '#title' => t('Pago_soportado'),
    '#default_value' => (isset($bank_defaults->Pago_soportado)) ? $bank_defaults->Pago_soportado: 'SSL',
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => TRUE,
  );
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => $bank_defaults->enabled,
    '#required' => TRUE,
  );  
  $form['encryption'] = array(
    '#type' => 'radios',
    '#title' => t('Method of encryption'),
    '#default_value' => $bank_defaults->encryption,
    '#options' => array(
      'sha1' => t('SHA'),
      'sha1-ampliado' => t('SHA Ampliado'),
    ),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#suffix' => l(t('Cancel'), 'admin/store/settings/payment/edit/uc_ceca'),
  );          

  return $form;
}

/**
 * Submit for bank edit/add form.
 */
function uc_ceca_form_bank_submit($form, &$form_state) {
  uc_ceca_save_bank($form_state['values']);
  drupal_set_message(t('Bank saved correctly'));
  $form_state['redirect'] = 'admin/store/settings/payment/edit/uc_ceca';  
}

/**
 * Handle the restore/delete bank confirmation form.
 */
function uc_ceca_form_restore_delete_bank($form_state, $mode = NULL) {
  $form = array();
  $bankcode = arg(5);
  $bank = uc_ceca_get_bank($bankcode);  
  $form['bankname'] = array('#type' => 'value', '#value' => $bank->bankname);
  $form['bankcode'] = array('#type' => 'value', '#value' => $bank->bankcode);
  $form['bankmode'] = array('#type' => 'value', '#value' => $bank->bankmode);
  $form['mode'] = array('#type' => 'value', '#value' => $mode);
  if ($mode == 'restoretodefault') {
    $message = t('Are you sure you want to restore the configuration of %bank?', array('%bank' => $bank->bankname));
    $button = t('Restore settings');
  }
  elseif ($mode == 'delete') {
    $message = t('Are you sure you want to delete %bank in %mode mode?', array('%bank' => $bank->bankname, '%mode' => $bank->bankmode));
    $button = t('Delete');
  }
  $caption .= '<p>'. t('This action cannot be undone.') .'</p>';
  return confirm_form($form, $message, 'admin/store/settings/payment/edit/uc_ceca', $caption, $button);  
}

function uc_ceca_form_restore_delete_bank_submit($form, &$form_state) {
  if ($form_state['values']['mode'] == 'delete') {
    uc_ceca_delete_bank($form_state['values']['bankcode']);
    drupal_set_message(t('%bank in %mode mode deleted', array('%bank' => $form_state['values']['bankname'], '%mode' => $form_state['values']['bankmode'])));  
  }
  elseif ($form_state['values']['mode'] == 'restoretodefault') {
    $bank = uc_ceca_default_banks($form_state['values']['bankcode']);
    uc_ceca_delete_bank($form_state['values']['bankcode']);
    drupal_write_record('uc_ceca_settings', $bank);
    drupal_set_message(t('The default settings of %bankname have been restored', array('%bankname' => $form_state['values']['bankname'])));
  }
  $form_state['redirect'] = 'admin/store/settings/payment/edit/uc_ceca';
}
